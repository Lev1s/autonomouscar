import java.util.ArrayList;
import java.util.List;

public class CarService extends Car {


    public CarInf getInfo(Car car) {
        return car.getCarInf();
    }

    public String sendInfo(Car car) {
        return "Info Sent";
    }

    public void setCurrSpeed(Car car, int currentSpeed) {
        car.setCurrSpeed(currentSpeed);
    }

    public double calculateBetweenCars(Car firstCar, Car secondCar) {
        //This formule i got from https://www.baeldung.com/java-distance-between-two-points
        // and i formatted like this, for best visualizing
        return Math
                .sqrt((secondCar.getLon() - secondCar.getLat()) * (secondCar.getLon() - secondCar.getLat()) +
                        (firstCar.getLon() - firstCar.getLat()) * (firstCar.getLon() - firstCar.getLat()));
    }

    public List<Car> closestCar(Car myCar, List<Car> carList) {
        List<Car> closestCarList = new ArrayList<>();
        carList.forEach(item -> {
            double minDistance = calculateBetweenCars(myCar, myCar);
            if (item.getLat() <= myCar.getLat() && item.getLon() <= myCar.getLon()) {
                double diffLon = Math.abs(item.getLon() - myCar.getLon());
                double diffLat = Math.abs(item.getLat() - myCar.getLat());
                if (diffLat < minDistance && diffLon < minDistance)
                    closestCarList.add(item);
            }
        });
        return closestCarList;
    }


}
