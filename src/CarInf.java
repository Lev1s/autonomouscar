public class CarInf {
    private int id;
    private String model;
    private String manufacturer;

    public CarInf(int id, String model, String manufacturer) {
        this.id = id;
        this.model = model;
        this.manufacturer = manufacturer;
    }

    public CarInf() {
    }

    @Override
    public String toString() {
        return "CarInf{" +
                "id=" + id +
                ", model='" + model + '\'' +
                ", manufacturer='" + manufacturer + '\'' +
                '}';
    }
}
