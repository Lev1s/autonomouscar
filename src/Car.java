public class Car {

    private CarInf carInf;
    private int currSpeed;
    private double lat;
    private double lon;
    private String events;


    public Car(CarInf carInf, int currSpeed, double lat, double lon, String events) {
        this.carInf = carInf;
        this.currSpeed = currSpeed;
        this.lat = lat;
        this.lon = lon;
        this.events = events;
    }

    public Car() {
    }

    public CarInf getCarInf() {
        return carInf;
    }

    public void setCurrSpeed(int currSpeed) {
        this.currSpeed = currSpeed;
    }

    public double getLat() {
        return lat;
    }

    public double getLon() {
        return lon;
    }

    @Override
    public String toString() {
        return "Car{" +
                "carInf=" + carInf +
                ", currSpeed=" + currSpeed +
                ", lat=" + lat +
                ", lon=" + lon +
                ", events='" + events + '\'' +
                '}';
    }
}
